// variables2.rs
// Make me compile! 

fn main() {
    // let x; // type annotations needed
    // let x:i32; // use of possibly uninitialized variable: `x`
    let x:i32 = 10; 
    // let x = "10"; // can't compare `&str` with `{integer}`
    
    if x == 10 {
        println!("Ten!");
    } else {
        println!("Not ten!");
    }
}