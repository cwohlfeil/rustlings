// ex3.rs
// Make me compile!

// May have gone a bit overboard by actually implementing a fmt::Debug but #[derive(Debug)] output looked ugly

use std::fmt;

struct Foo {
    capacity: i32,
}

impl fmt::Debug for Foo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Capacity: {}", self.capacity)
    }
}

fn main() {
    println!("{:?}", Foo { capacity: 3 });
}
