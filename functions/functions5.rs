// functions5.rs
// Make me compile! Scroll down for hints :)

fn main() {
    let answer = square(3);
    println!("The answer is {}", answer);
}

fn square(num: i32) -> i32 {
    num * num
}
// This happens because Rust distinguishes between expressions and statements. Expressions return
// a value and statements don't. Semicolon is only for statements.