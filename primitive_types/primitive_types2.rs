// primitive_types2.rs
// Fill in the rest of the line that has code missing!
// No hints, there's no tricks, just get used to typing these :)

fn main() {
    let my_first_initial = 'C';
    // Using regular if
    if my_first_initial.is_alphabetic() == true {
        println!("Alphabetical!");
    } else if my_first_initial.is_numeric() == true {
        println!("Numerical!");
    } else {
        println!("Neither alphabetic nor numeric!");
    }

    let your_character = 'Ö';
    // using match if
    match your_character {
        _ if your_character.is_alphabetic() => println!("Alphabetical!"),
        _ if your_character.is_numeric() => println!("Numerical!"),
        _ => println!("Neither alphabetic nor numeric!"),
    }
}
