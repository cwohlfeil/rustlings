// primitive_types6.rs
// Use a tuple index to access the second element of `numbers`.

fn main() {
    let numbers = (1, 2, 3);
    println!("The second number is {}", numbers.1);
}
