// ex2.rs
// Make me compile!

// This was surprisingly hard to understand, something() was returning a reference to a string literal instead of a String

fn something() -> String {
    String::from("hi!") // could also be done as compiler recommends: "hi!".to_string()
}

fn main() {
    println!("{}", something());
}
